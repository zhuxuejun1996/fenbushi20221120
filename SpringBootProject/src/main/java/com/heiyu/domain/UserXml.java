package com.heiyu.domain;

import lombok.Data;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @Description
 * @Author xuejun.zhu
 * @Date 2022-11-15 14:08
 */
@XmlRootElement(name="userxml")
@Data
public class UserXml{

    private String userName;

    private String sex;

    private int age;
}
