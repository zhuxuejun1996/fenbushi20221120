package com.heiyu.domain;

import lombok.Data;
/**
 * @Description
 * @Author xuejun.zhu
 * @Date 2022-11-15 13:59
 */
@Data
public class User {

    private String userName;

    private String sex;

    private int age;
}
