package com.heiyu.controller;

import com.heiyu.domain.User;
import com.heiyu.domain.UserXml;
import com.heiyu.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Description
 * @Author xuejun.zhu
 * @Date 2022-11-15 14:02
 */
@RestController
@RequestMapping("/api/user")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/list")
    public List<User> listUser(){
        return userService.listUser();
    }

    @GetMapping("/list/xml")
    public List<UserXml> listUserXml(){
        return userService.listUserXml();
    }
}
