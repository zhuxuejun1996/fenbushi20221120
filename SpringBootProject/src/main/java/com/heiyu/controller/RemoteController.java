package com.heiyu.controller;

import com.heiyu.service.RemoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description
 * @Author xuejun.zhu
 * @Date 2022-11-15 18:55
 */
@RestController
@RequestMapping("/api/remote")
public class RemoteController {

    @Autowired
    private RemoteService remoteService;

    /**
     * 发送数据到指定的url 实际场景会有调整，需要密钥等信息
     * @param url
     * @return
     */
    @GetMapping("/send")
    public String pushData(@RequestParam("url") String url){
       return remoteService.pushData(url);
    }
}
