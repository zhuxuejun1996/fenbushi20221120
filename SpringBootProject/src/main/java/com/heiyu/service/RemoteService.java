package com.heiyu.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * @Description
 * @Author xuejun.zhu
 * @Date 2022-11-15 18:42
 */
@Service
@Slf4j
public class RemoteService {

    /**
     * 远程调用
     *
     * @param
     * @return
     */
    public String pushData(String url) {
        String msg = null;
        String random = UUID.randomUUID().toString();
        String timestamp = Long.toString(System.currentTimeMillis());
        Map<String, Object> form = new HashMap<>();
        form.put("isEncrypt", false);
        form.put("random", random);
        form.put("timestamp", timestamp);
        HttpHeaders httpHeaders = new HttpHeaders();
        HttpEntity httpEntity = new HttpEntity<>(form, httpHeaders);
        log.info("接口请求参数：{}", httpEntity);
        try {
            String response = RestGeneral.sendData(url + "?sign=" , httpEntity
                    , "POST");
            log.info("response=:"+response);
        } catch (Exception e) {
           e.printStackTrace();

        }
        return msg;
    }

}
