package com.heiyu.service;

import com.heiyu.domain.User;
import com.heiyu.domain.UserXml;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @Description
 * @Author xuejun.zhu
 * @Date 2022-11-15 14:00
 */
@Service
public class UserService {

    public List<User> listUser(){
        ArrayList<User> users = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            User user = new User();
            user.setUserName("heiyu"+i);
            if(i % 2 ==0){
                user.setSex("男");
            }else{
                user.setSex("女");
            }
            user.setAge(i+10);
            users.add(user);
        }
        return users;
    }

    public List<UserXml> listUserXml(){
        ArrayList<UserXml> users = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            UserXml userxml = new UserXml();
            userxml.setUserName("heiyu"+i);
            if(i % 2 ==0){
                userxml.setSex("男");
            }else{
                userxml.setSex("女");
            }
            userxml.setAge(i+10);
            users.add(userxml);
        }
        return users;
    }
}
