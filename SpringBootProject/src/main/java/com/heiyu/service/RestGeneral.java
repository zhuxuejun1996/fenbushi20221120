package com.heiyu.service;

import com.heiyu.exception.BizException;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.SocketAddress;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

/**
 * @Description 远程调用工具类
 * @Author xuejun.zhu
 * @Date 2022-11-15 18:43
 */
@Component
@Slf4j
public class RestGeneral {

    @Autowired
    private RestTemplate restTemplateProxy;

    private static RestTemplate restTemplate2;

    @PostConstruct
    public void init() {
        SimpleClientHttpRequestFactory simpleClientHttpRequestFactory = new SimpleClientHttpRequestFactory();
        simpleClientHttpRequestFactory.setReadTimeout(5000);
        simpleClientHttpRequestFactory.setConnectTimeout(5000);
        SocketAddress address = new InetSocketAddress("192.168.100.253",443);
        Proxy proxy = new Proxy(Proxy.Type.HTTP,address);
        simpleClientHttpRequestFactory.setProxy(proxy);
        restTemplate2 = new RestTemplate(simpleClientHttpRequestFactory);
    }

    public static String sendData(String url, HttpEntity httpEntity, String method) {
        log.debug("发送报文url:{},消息体:{}",url,httpEntity.getBody());
        String msg;
        HttpClient httpClient = HttpClientBuilder.create().build();
        ClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory(httpClient);
        RestTemplate restTemplate = new RestTemplate(requestFactory);
        try {
            URL u = new URL(url);
            Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress("192.168.100.253",443));
            HttpURLConnection con = (HttpURLConnection) u.openConnection(proxy);
            if ("POST".equals(method)) {
                msg = restTemplate.exchange(url, HttpMethod.POST, httpEntity, String.class).getBody();
            } else {
                msg = restTemplate.exchange(url, HttpMethod.GET, httpEntity, String.class).getBody();
            }
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            log.error("执行错误！访问外部接口时发生错误！{}",
                    (StringUtils.isEmpty(e.getResponseBodyAsString())
                            ? e : e.getResponseBodyAsString()));
            throw new BizException("REQUEST_FAILED");
        } catch (Exception e) {
            throw new BizException("REQUEST_FAILED");
        }
        return msg;
    }


    public static HttpHeaders setHeader() {
        //设置头
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Content-Type", "application/json;charset=UTF-8");
        return httpHeaders;
    }

    public static String getMD5Hash(String source) {
        StringBuilder sb = new StringBuilder();
        MessageDigest md5;
        try {
            md5 = MessageDigest.getInstance("MD5");
            md5.update(source.getBytes());
            for (byte b : md5.digest()) {
                sb.append(String.format("%02x", b));
            }
            return sb.toString();
        } catch (NoSuchAlgorithmException e) {
            log.error("没有这样的算法异常: [{}]", e);
        }
        return null;
    }




    /**
     * 去掉url中的路径，留下请求参数部分
     *
     * @param strURL url地址
     * @return url请求参数部分
     * @author lzf
     */
    private static String TruncateUrlPage(String strURL) {
        String strAllParam = null;
        String[] arrSplit;
        strURL = strURL.trim().toLowerCase();
        arrSplit = strURL.split("[?]");
        if (strURL.length() > 1) {
            if (arrSplit.length > 1) {
                for (int i = 1; i < arrSplit.length; i++) {
                    strAllParam = arrSplit[i];
                }
            }
        }
        return strAllParam;
    }

    /**
     * 解析出url参数中的键值对
     * 如 "index.jsp?Action=del&id=123"，解析出Action:del,id:123存入map中
     *
     * @param URL url地址
     * @return url请求参数部分
     * @author lzf
     */
    public static Map<String, String> urlSplit(String URL) {
        Map<String, String> mapRequest = new HashMap();
        String[] arrSplit;
        String strUrlParam = TruncateUrlPage(URL);
        if (strUrlParam == null) {
            return mapRequest;
        }
        arrSplit = strUrlParam.split("[&]");
        for (String strSplit : arrSplit) {
            String[] arrSplitEqual = strSplit.split("[=]");
            //解析出键值
            if (arrSplitEqual.length > 1) {
                //正确解析
                mapRequest.put(arrSplitEqual[0], arrSplitEqual[1]);
            } else {
                if ("".equals(arrSplitEqual[0])) {
                    //只有参数没有值，不加入
                    mapRequest.put(arrSplitEqual[0], "");
                }
            }
        }
        return mapRequest;
    }

}

