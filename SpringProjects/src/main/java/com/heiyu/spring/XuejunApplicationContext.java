package com.heiyu.spring;

import java.io.File;
import java.lang.reflect.Field;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Description
 * @Author xuejun.zhu
 * @Date 2022-11-15 11:00
 */
public class XuejunApplicationContext {

    public Class configClass;

    public Map<String,BeanDefinition> beanDefinitionMap = new HashMap<>();
    private Map<String, Object> singletonObjects = new HashMap<>();
    private List<BeanPostProcessor> beanPostProcessorList =new ArrayList<>();


    public XuejunApplicationContext(Class configClass){
        this.configClass = configClass;

        scan(configClass);

        if(beanDefinitionMap.size() > 0){
            for (Map.Entry<String, BeanDefinition> entry : beanDefinitionMap.entrySet()) {
                String beanName = entry.getKey();
                BeanDefinition beanDefinition = entry.getValue();
                if (beanDefinition.getScope().equals("singleton")) {

                    Object bean = createBean(beanName, beanDefinition);
                    singletonObjects.put(beanName, bean);

                }
            }
        }
    }

    public Object createBean(String beanName,BeanDefinition beanDefinition){
        Class clazz = beanDefinition.getType();

        Object instance = null;
        try {
            instance = clazz.getConstructor().newInstance();
            for(Field field : clazz.getDeclaredFields()){
                if (field.isAnnotationPresent(Autowired.class)) {
                    field.setAccessible(true);
                    field.set(instance,getBean(field.getName()));
                }

            }
            if (instance instanceof BeanNameAware) {
                ((BeanNameAware)instance).setBeanName(beanName);
            }

            for (BeanPostProcessor beanPostProcessor : beanPostProcessorList){
                instance = beanPostProcessor.postProcessBeforeInitialization(instance, beanName);
            }

            for (BeanPostProcessor beanPostProcessor : beanPostProcessorList) {
                instance = beanPostProcessor.postProcessAfterInitialization(instance, beanName);
            }






        } catch (Exception e) {
            e.printStackTrace();
        }
        return instance;
    }

    public Object getBean(String beanName){
        if(null != beanDefinitionMap && beanDefinitionMap.size() != 0){
            if (!beanDefinitionMap.containsKey(beanName)) {
                throw new NullPointerException();
            }
        }
        BeanDefinition beanDefinition = beanDefinitionMap.get(beanName);
        if (beanDefinition.getScope().equals("singleton")) {
            Object singletonBean = singletonObjects.get(beanName);
            if (singletonBean == null) {
                singletonBean = createBean(beanName, beanDefinition);
                singletonObjects.put(beanName, singletonBean);
            }
            return singletonBean;
        } else {
            // 原型
            Object prototypeBean = createBean(beanName, beanDefinition);
            return prototypeBean;
        }
    }


    private void scan(Class configClass) {
        // 判断是否是组件注解
        if (configClass.isAnnotationPresent(ComponentScan.class)) {
            ComponentScan componentScanAnnotation = (ComponentScan) configClass.getAnnotation(ComponentScan.class);
            String path = componentScanAnnotation.value(); //com.heiyu.xuejun.service
            System.out.println(path); //com.heiyu.xuejun.service
            path = path.replace(".","/");

            ClassLoader classLoader = configClass.getClassLoader();
            URL resource = classLoader.getResource(path);
            File file = new File(resource.getFile());
            if(file.isDirectory()){
                for (File f : file.listFiles()) {
                    String absolutePath = f.getAbsolutePath();
                    System.out.println(absolutePath);
                    // com.heiyu.xuejun.service
                    absolutePath = absolutePath.substring(absolutePath.indexOf("com"), absolutePath.indexOf(".class"));
                    absolutePath = absolutePath.replace("\\", ".");

                    System.out.println(absolutePath);

                    try {
                        Class<?> clazz = classLoader.loadClass(absolutePath);
                        if (clazz.isAnnotationPresent(Component.class)) {

                            if (BeanPostProcessor.class.isAssignableFrom(clazz)) { // 判断clazz是否实现BeanPostProcessor接口
                                BeanPostProcessor instance = (BeanPostProcessor) clazz.getConstructor().newInstance();
                                beanPostProcessorList.add(instance);
                            }

                            BeanDefinition beanDefinition = new BeanDefinition();
                            Component componentAnnotation = clazz.getAnnotation(Component.class);
                            String beanName = componentAnnotation.value();
                            beanDefinition.setName(beanName);
                            beanDefinition.setType(clazz);

                            if(clazz.isAnnotationPresent(Scope.class)){
                                Scope annotation = clazz.getAnnotation(Scope.class);
                                beanDefinition.setScope(annotation.value());
                            }else{
                                beanDefinition.setScope("singleton");
                            }
                            beanDefinitionMap.put(beanName,beanDefinition);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }
            }


        }
    }
}
