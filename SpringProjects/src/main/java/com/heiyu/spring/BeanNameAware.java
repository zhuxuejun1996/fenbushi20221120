package com.heiyu.spring;

/**
 * @Description
 * @Author xuejun.zhu
 * @Date 2022-11-15 10:57
 */
public interface BeanNameAware {
    void setBeanName(String name);
}
