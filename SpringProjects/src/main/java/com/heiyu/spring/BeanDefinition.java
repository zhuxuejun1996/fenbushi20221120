package com.heiyu.spring;

/**
 * @Description
 * @Author xuejun.zhu
 * @Date 2022-11-15 11:00
 */
public class BeanDefinition {

    private Class type;

    private String name;

    private String scope;

    private boolean isLazy;

    public Class getType() {
        return type;
    }

    public void setType(Class type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public boolean isLazy() {
        return isLazy;
    }

    public void setLazy(boolean lazy) {
        isLazy = lazy;
    }
}
