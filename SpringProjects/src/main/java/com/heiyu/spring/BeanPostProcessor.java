package com.heiyu.spring;

/**
 * @Description
 * @Author xuejun.zhu
 * @Date 2022-11-15 11:01
 */
public interface BeanPostProcessor {

    default Object postProcessBeforeInitialization(Object bean, String beanName) {
        return bean;
    }

    default Object postProcessAfterInitialization(Object bean, String beanName) {
        return bean;
    }
}
