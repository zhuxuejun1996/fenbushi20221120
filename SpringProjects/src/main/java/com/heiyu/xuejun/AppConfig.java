package com.heiyu.xuejun;

import com.heiyu.spring.ComponentScan;

/**
 * @Description
 * @Author xuejun.zhu
 * @Date 2022-11-15 11:02
 */
@ComponentScan("com.heiyu.xuejun.service")
public class AppConfig {
}
