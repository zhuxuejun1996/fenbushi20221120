//package com.heiyu.xuejun.service;
//
//
//import com.heiyu.spring.BeanPostProcessor;
//import com.heiyu.spring.Component;
//import com.heiyu.spring.XuejunValue;
//
//import java.lang.reflect.Field;
//
//@Component
//public class xuejunBeanPostProcessor implements BeanPostProcessor {
//
//    @Override
//    public Object postProcessBeforeInitialization(Object bean, String beanName) {
//        for (Field field : bean.getClass().getFields()){
//            if (field.isAnnotationPresent(XuejunValue.class)) {
//                XuejunValue annotation = field.getAnnotation(XuejunValue.class);
//                field.setAccessible(true);
//                try {
//                    field.set(bean,annotation.value());
//                } catch (IllegalAccessException e) {
//                    e.printStackTrace();
//                }
//            }
//        }
//        return bean;
//    }
//
//
//}
