//package com.heiyu.xuejun.service;
//
//
//import com.heiyu.spring.BeanPostProcessor;
//import com.heiyu.spring.Component;
//
//import java.lang.reflect.InvocationHandler;
//import java.lang.reflect.Method;
//import java.lang.reflect.Proxy;
//
//@Component
//public class xuejunBeanBeforeProcessor implements BeanPostProcessor {
//
//    @Override
//    public Object postProcessAfterInitialization(Object bean, String beanName) {
//
//        if(beanName.equals("userService")){
//            Object proxyInstance = Proxy.newProxyInstance(xuejunBeanBeforeProcessor.class.getClassLoader(), bean.getClass().getInterfaces(), new InvocationHandler() {
//                @Override
//                public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
//                    System.out.println("执行切面逻辑");
//                    return method.invoke(bean,args);
//                }
//            });
//            return proxyInstance;
//        }
//
//        return bean;
//    }
//
//
//
//}
