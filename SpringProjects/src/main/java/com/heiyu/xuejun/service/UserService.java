package com.heiyu.xuejun.service;

import com.heiyu.spring.Autowired;
import com.heiyu.spring.BeanNameAware;
import com.heiyu.spring.Component;

/**
 * @Description
 * @Author xuejun.zhu
 * @Date 2022-11-15 10:57
 */
@Component("userService")
public class UserService implements UserInterface, BeanNameAware {

    @Autowired
    private OrderService orderService;

    private String beanName;

    @Override
    public void Test(){
        System.out.println("---------> test"+beanName);
    }

    @Override
    public void setBeanName(String name) {
        this.beanName = name;
    }
}
