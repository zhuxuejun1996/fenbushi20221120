package com.heiyu.xuejun.service;

import com.heiyu.spring.Component;
import com.heiyu.xuejun.domain.Order;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * @Description
 * @Author xuejun.zhu
 * @Date 2022-11-15 10:56
 */
@Component("orderService")
public class OrderService {

    public void test(){
        System.out.println("order");
    }

    public List<Order> listOrder(){
        ArrayList<Order> orders = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            Order order = new Order();
            order.setName("heiyu"+i);
            order.setAmount(new BigDecimal(i+10));
            orders.add(order);
        }
        return orders;
    }
}
