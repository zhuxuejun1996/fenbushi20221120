package com.heiyu.xuejun.domain;
import java.math.BigDecimal;

/**
 * @Description
 * @Author xuejun.zhu
 * @Date 2022-11-15 11:22
 */
public class Order {

    /**
     * 名称
     */
    private String name;

    /**
     * 金额
     */
    private BigDecimal amount;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}
