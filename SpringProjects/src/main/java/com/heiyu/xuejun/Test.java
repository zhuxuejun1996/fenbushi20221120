package com.heiyu.xuejun;

import com.heiyu.spring.XuejunApplicationContext;
import com.heiyu.xuejun.service.UserInterface;

/**
 * @Description
 * @Author xuejun.zhu
 * @Date 2022-11-15 11:00
 */
public class Test {

    public static void main(String[] args) {

        // 扫描--->创建单例Bean BeanDefinition BeanPostPRocess
        // 自定义
        XuejunApplicationContext xuejunApplicationContext = new XuejunApplicationContext(AppConfig.class);

        UserInterface userService = (UserInterface) xuejunApplicationContext.getBean("userService");
        userService.Test();

    }
}
