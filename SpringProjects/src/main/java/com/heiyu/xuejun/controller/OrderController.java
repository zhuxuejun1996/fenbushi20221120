package com.heiyu.xuejun.controller;

import com.heiyu.xuejun.domain.Order;
import com.heiyu.xuejun.service.OrderService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Description
 * @Author xuejun.zhu
 * @Date 2022-11-15 11:22
 */
@RestController
@RequestMapping("/api")
public class OrderController {

    private OrderService orderService;

    @GetMapping("/query")
    public List<Order> listOrder(){
        return orderService.listOrder();
    }
}
